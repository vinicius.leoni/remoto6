#Questao1
def altera_frase(fras,palav,pos):
    '''função que transforma uma palavra identificada na frase em caixa alta, e, caso ela não exista, ela será inserida na posição desejada
	str,str,int -> str'''
    separado = fras.split()
    if palav in separado:
        repet = separado.index(palav)
        separado[repet] = str.upper(palav)
        return str.join(' ',separado)
    else:
        list.insert(separado,pos,palav)
        alterada = str.join(' ',separado)
        return alterada
#Questão2
def faltas (Score):
    
    '''função que retorna a soma total de faltas em um campeonato de 3 jogos'''
    '''list->int'''
    Jogo1 = Score[0]
    Jogo2 = Score[1]
    Jogo3 = Score[2]
    faltasJogo1 = Jogo1[2]
    faltasJogo2 = Jogo2[2]
    faltasJogo3 = Jogo3[2]
    somaFaltasJogo1 = faltasJogo1[0]+faltasJogo1[1]
    somaFaltasJogo2 = faltasJogo2[0]+faltasJogo2[1]
    somaFaltasJogo3 = faltasJogo3[0]+faltasJogo3[1]
    totalFaltas = somaFaltasJogo1+somaFaltasJogo2+somaFaltasJogo3
    return totalFaltas

#Questão3
def insere(lista_numero, n):
    '''função que retorna uma lista ordenada após acrescenter um valor n'''
    '''list,int->list'''
    list.insert(lista_numero,0,n)
    list.sort(lista_numero)
    
    return lista_numero
#Questão4
def maiores(lista, n):
    '''Função que retorna uma lista ordenada com os números maiores que n fornecidos'''
    '''list, int -> list'''
    if n not in lista:
    	list.insert(lista,0,n)
    list.sort(lista)
    local_De_n = list.index(lista,n)
    nobalist = lista[local_De_n+1:]
    return nobalist

#Questão5
def acima_da_media(notas):
    '''função que retorna uma lista com as notas acima da média calculadas entre elas mesmas'''
    media = sum(notas)/len(notas)
    return maiores(notas, media)

#Questão6
def eh_ordenada(lista):
    '''Função que retorna para qual condição entre crescente e decrescente a lista apresenta verdadeiro, caso contrário alertará falso e desordenado'''
    '''list->tuple'''
    
    tupla =()
    listaOriginal = lista[:]
    list.sort(lista)
    lista1 = lista[:] 
    list.reverse(lista)
    lista2 = lista[:]
    
    if listaOriginal == lista1:
        tupla = tupla + (True,'crescente')
        return tupla
    elif listaOriginal == lista2:
        tupla = tupla + (True,'decrescente')
        return tupla
    elif listaOriginal != lista1 and listaOriginal !=  lista2:
    	tupla = tupla + (False,'desordenada')
    	return tupla
        
